<style>
    .card {
        width: 375px;
        margin: 10px;
        box-shadow: 1px 1px 5px grey;
        padding: 1.5rem;
        border-radius: 0.5rem;
    }
    .card.deleted {
        opacity: 0.5;
    }
</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-right text-xl text-gray-800 leading-tight">
            <x-nav-link :href="route('notifications')" :active="request()->routeIs('notifications')">
                {{ __('Notifications') }}
                <h1 class="text-red-900 font-bold p-1 ml-2">{{$notificationCount}}</h1>
            </x-nav-link>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div  class="bg-white pb-3 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-gray-400 border-b border-gray-200">
                    <h1>Welcome {{ $currentUser->name }} !!</h1>
                </div>

                <h1 style="font-size: 20px;" class=" py-5 text-center uppercase">All Users</h1>
                <div class="p-3 flex flex-wrap">
                    @foreach($users as $user)
                        @if($user == $currentUser)
                            <div class="card bg-gray-200">
                                <h1>Name: {{ $user->name }} (You)</h1>
                                <h1>Email: {{ $user->email }} </h1>
                                <h1>Username: {{ $user->username }} </h1>
                                <h1>Phone no: {{ $user->phone }} </h1>
                                <h1>Birth Date: {{ $user->date_of_birth }} </h1>
                                <h1>Last login: {{ $user->last_login }} </h1>
                                <div>
                                    <h1 class="">Languages Known: </h1>
                                    @foreach($user->languages_known as $language)
                                        <span style="color: #8d1414"> {{ $language }}, </span>
                                    @endforeach
                                </div>
{{--                                <a href="" class="ac-button">Delete User</a>--}}
                            </div>
                        @elseif($user->deleted_at != '')
                            <div class="card deleted">
                                <h1>Name: {{ $user->name }} </h1>
                                <h1>Email: {{ $user->email }} </h1>
                                <h1>Username: {{ $user->username }} </h1>
                                <h1>Phone no: {{ $user->phone }} </h1>
                                <h1>Birth Date: {{ $user->date_of_birth }} </h1>
                                <h1>Last login: {{ $user->last_login }} </h1>
                                <div>
                                    <h1 class="">Languages Known: </h1>
                                    @foreach($user->languages_known as $language)
                                        <span style="color: #8d1414"> {{ $language }}, </span>
                                    @endforeach
                                </div>
{{--                                <a href=""  class="ac-button">Delete User</a>--}}
                            </div>
                        @else
                            <div class="card bg-gray-50">
                                <h1>Name: {{ $user->name }} </h1>
                                <h1>Email: {{ $user->email }} </h1>
                                <h1>Username: {{ $user->username }} </h1>
                                <h1>Phone no: {{ $user->phone }} </h1>
                                <h1>Birth Date: {{ $user->date_of_birth }} </h1>
                                <h1>Last login: {{ $user->last_login }} </h1>
                                <div>
                                    <h1 class="">Languages Known: </h1>
                                    @foreach($user->languages_known as $language)
                                        <span style="color: #8d1414"> {{ $language }}, </span>
                                    @endforeach
                                </div>
                                {{--                                <a href=""  class="ac-button">Delete User</a>--}}
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
