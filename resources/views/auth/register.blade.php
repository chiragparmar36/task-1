<style>
    .error-message {
        color: red;
    }
</style>
<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
{{--        <x-auth-validation-errors class="mb-4" :errors="$errors" />--}}

        <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
            @csrf

            <!-- Name -->
            <div>
                <x-label for="name" :value="__('Name')" />

                <x-input id="name"
                         class="block mt-1 w-full @error('name') is-invalid @enderror"
                         type="text" name="name" :value="old('name')" required autofocus />
                @error('name')
                <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />

                <x-input id="email"
                         class="block mt-1 w-full @error('email') is-invalid @enderror"
                         type="email" name="email" :value="old('email')" required />
                @error('email')
                <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <!-- Username -->
            <div>
                <x-label for="username" :value="__('Username')" />

                <x-input id="username"
                         class="block mt-1 w-full @error('username') is-invalid @enderror"
                         type="text" name="username" :value="old('username')" required autofocus />
                @error('username')
                <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password"
                         class="block mt-1 w-full @error('password') is-invalid @enderror"
                         type="password"
                         name="password"
                         required autocomplete="new-password" />
                @error('password')
                <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-input id="password_confirmation" class="block mt-1 w-full"
                                type="password"
                                name="password_confirmation" required />
            </div>

            <!-- Phone No -->
            <div class="mt-4">
                <x-label for="phone" :value="__('Phone')" />

                <x-input id="phone"
                         class="block mt-1 w-full @error('phone') is-invalid @enderror"
                         type="text" :value="old('phone')"
                         name="phone" required />
                @error('phone')
                <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <!-- Date of Birth -->
            <div class="mt-4">
                <x-label for="date_of_birth" :value="__('Date of Birth')" />

                <x-input id="date_of_birth"
                         class="block mt-1 w-full @error('date_of_birth') is-invalid @enderror"
                         type="date" :value="old('date_of_birth')"
                         name="date_of_birth" required></x-input>
                @error('date_of_birth')
                <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="mt-4">
                <x-label for="languages_known" :value="__('Known Languages')" />
                <select id="languages_known" name="languages_known[]"
                        class="block mt-1 w-full @error('languages_known') is-invalid @enderror" data-live-search="true" multiple>
                    <option value="hindi">Hindi</option>
                    <option value="english">English</option>
                    <option value="gujarati">Gujarati</option>
                </select>
                @error('languages_known')
                <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="mt-4">
                <x-label for="interest" :value="__('Interest')" />
                <select id="interest" name="interest[]"
                        class="block mt-1 w-full @error('interest') is-invalid @enderror" data-live-search="true" multiple>
                    @foreach($tags as $tag)
                        <option value="{{$tag->name}}">{{$tag->name}}</option>
                    @endforeach
                </select>
                @error('interest')
                <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <!-- Profile pic -->
            <div class="mt-4">
                <x-label for="profilePic" :value="__('Profile Pic')" />
                <input type="file" name="profilePic" id="profilePic" class="block mt-1 w-full" />

                @error('profilePic')
                <span class="mt-3 list-disc list-inside text-sm text-red-600">{{ $message }}</span>
                @enderror
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-button class="ml-4">
                    {{ __('Register') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
