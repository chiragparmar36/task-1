<style>
    .card {
        margin: 10px;
        box-shadow: 1px 1px 5px grey;
        padding: 1.5rem;
        border-radius: 0.5rem;
    }
    .btn {
        display: block;
        color: white;
        padding: 10px 16px;
        border-radius: 5px;
        width: 130px;
        box-sizing: border-box;
        margin-top: 10px;
        cursor: pointer;
        text-align: center;
    }

    .btn.btn-reply {
        background: rgba(77, 79, 88, 0.76);
    }

</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-right text-xl text-gray-800 leading-tight">
            <x-nav-link :href="route('notifications')" :active="request()->routeIs('notifications')">
                {{ __('Notifications') }}
                <h1 class="text-red-900 font-bold p-1 ml-2">{{$notificationCount}}</h1>
            </x-nav-link>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div  class="bg-white pb-3 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-gray-400 border-b border-gray-200">
                    <h1>Welcome {{ $username }} !!</h1>
                </div>
                <h1 style="font-size: 20px;" class=" py-5 text-center uppercase">All Users</h1>
                <div class="p-3 card">
                    <div class="flex justify-between bg-red-100 p-3 m-4" style="align-items: center">
                        <h1 class="text-red-900 font-bold ">Unread Notifications</h1>
                        <a class="bg-red-900 text-white py-2 px-4 rounded" href={{ route('notifications.markasreadall') }} >Read All</a>
                    </div>
                    @foreach($unreadNotifications as $notification)
                        @if($notification->type == 'App\Notifications\CommentCreateNotification')
                        <div class="notification-card shadow-sm rounded p-3 m-4 bg-gray-200">
                            <h1 class="font-bold my-2">{{$username}}</h1>
                            <h1 class="my-2">{{$notification->data['username']}} Has Comment on Your Post</h1>
                            <h1>Title: {{$notification->data['title']}}</h1>
                            <div class="flex">
                                <a class="btn btn-reply" href={{ route('posts.view', ['post' => $notification->data['id']]) }}>Show Post</a>
                                <a class="btn-reply ml-3 btn" href={{ route('notifications.markasread', ['id' => $notification->id]) }} >Read</a>
                            </div>
                        </div>
                        @else
                            <div class="notification-card shadow-sm rounded p-3 m-4 bg-gray-200">
                                <h1 class="font-bold my-2">{{$username}}</h1>
                                <h1 class="my-2">{{$notification->data['username']}} Has Reply on Your Comment</h1>
                                <h1>Title: {{$notification->data['body']}}</h1>
                                <div class="flex">
                                    <a class="btn btn-reply" href={{ route('posts.view', ['post' => $notification->data['id']]) }}>View Post</a>
                                    <a class="btn-reply ml-3 btn" href={{ route('notifications.markasread', ['id' => $notification->id]) }} >Read</a>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="flex justify-between bg-green-100 p-3 m-4" style="align-items: center">
                        <h1 class="text-green-900 font-bold ">Readed Notifications</h1>
                        <a class="bg-green-900 text-white py-2 px-4 rounded" href={{ route('notifications.delete') }} >Delete</a>
                    </div>
                    @foreach($readNotifications as $notification)
                        @if($notification->type == 'App\Notifications\CommentCreateNotification')
                        <div class="notification-card shadow-sm rounded p-3 m-4 bg-gray-200">
                            <h1 class="font-bold my-2">{{$username}}</h1>
                            <h1 class="my-2">{{$notification->data['username']}} Has Comment on Your Post</h1>
                            <h1>Title: {{$notification->data['title']}}</h1>
                            <a class="btn btn-reply" href={{ route('posts.view', ['post' => $notification->data['id']]) }}>Show Post</a>
                        </div>
                        @else
                            <div class="notification-card shadow-sm rounded p-3 m-4 bg-gray-200">
                                <h1 class="font-bold my-2">{{$username}}</h1>
                                <h1 class="my-2">{{$notification->data['username']}} Has Reply on Your Comment</h1>
                                <h1>Title: {{$notification->data['body']}}</h1>
                                <a class="btn btn-reply" href={{ route('posts.view', ['post' => $notification->data['id']]) }}>Show Post</a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
