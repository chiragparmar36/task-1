

<style>
    .desc {
        height: 200px;
        border: 1px solid lightgrey;
        border-radius: 5px;
    }
</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-right text-xl text-gray-800 leading-tight">
            <x-nav-link :href="route('posts.create')" :active="request()->routeIs('posts.create')">
                {{ __('Add Post') }}
            </x-nav-link>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-4xl mx-auto sm:px-6 lg:px-8">
            <div  class="bg-white pb-3 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-gray-400 border-b border-gray-200">
                    <h1 class="uppercase text-center text-2xl">Change Password</h1>
                </div>
                <div class="p-10">
                    <x-auth-card>
                        <x-slot name="logo">
                            <a href="/">
                                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
                            </a>
                        </x-slot>

                        <!-- Session Status -->
                        <x-auth-session-status class="mb-4" :status="session('status')" />

                        <!-- Validation Errors -->
                        <x-auth-validation-errors class="mb-4" :errors="$errors" />

                        <form method="POST" action="{{ route('account.storechangepassword') }}">
                        @csrf

                        <!-- Password -->
                            <div class="mt-4">
                                <x-label for="oldpassword" :value="__('Old Password')" />

                                <x-input id="oldpassword" class="block mt-1 w-full"
                                         type="password"
                                         name="oldpassword"
                                         required />
                            </div>

                            <!-- Password -->
                            <div class="mt-4">
                                <x-label for="password" :value="__('Password')" />

                                <x-input id="password" class="block mt-1 w-full"
                                         type="password"
                                         name="password"
                                         required autocomplete="current-password" />
                            </div>

                            <!-- Password -->
                            <div class="mt-4">
                                <x-label for="password_confirmation" :value="__('Confirm Password')" />

                                <x-input id="password_confirmation" class="block mt-1 w-full"
                                         type="password"
                                         name="password_confirmation"
                                         required />
                            </div>

                            <div class="flex items-center justify-end mt-4">
                                <x-button class="ml-3">
                                    {{ __('Change') }}
                                </x-button>
                            </div>
                        </form>
                    </x-auth-card>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>



