<style>
    .card {
        width: 600px;
        margin: 10px auto;
        box-shadow: 1px 1px 5px grey;
        padding: 1.5rem;
        border-radius: 0.5rem;
    }
    h1.languages {
        box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05);
        border-radius: 0.375rem;
        background-color: #fff;
        border: 1px solid lightgray;
        padding: 0.5rem 0.75rem;
        font-size: 1rem;
        line-height: 1.5rem;
        margin-top: 5px;
    }
    .bt {
        display: block;
        background: #2d3748;
        color: white;
        padding: 10px 20px;
        width: max-content;
        border-radius: 5px;
    }
    .profilePic {
        height: 250px;
        margin: auto;
        border-radius: 10px;
    }
</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-right text-xl text-gray-800 leading-tight">
            <x-nav-link :href="route('posts.create')" :active="request()->routeIs('posts.create')">
                {{ __('Add Post') }}
            </x-nav-link>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div  class="bg-white pb-3 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-gray-400 border-b border-gray-200">
                    <h1>Welcome {{ $user->name }} !!</h1>
                </div>
                <h1 style="font-size: 20px;" class=" py-5 text-center uppercase">My Account</h1>
                <div class="card bg-gray-200">
                    <div class="mt-4">
                        <img class="profilePic" src="/storage/images/profiles/{{$user->profilePic}}" class="profilePic" />
                    </div>
                    <div class="mt-4">
                        <h1 class="my-label">Name</h1>
                        <h1 class="languages">{{$user->name}}</h1>
                    </div>
                    <div class="mt-4">
                        <h1 class="my-label">Email</h1>
                        <h1 class="languages">{{$user->email}}</h1>
                    </div>
                    <div class="mt-4">
                        <h1 class="my-label">Username</h1>
                        <h1 class="languages">{{$user->username}}</h1>
                    </div>
                    <div class="mt-4">
                        <h1 class="my-label">Phone</h1>
                        <h1 class="languages">{{$user->phone}}</h1>
                    </div>
                    <div class="mt-4">
                        <h1 class="my-label">Birth Date</h1>
                        <h1 class="languages">{{$user->date_of_birth}}</h1>
                    </div>
                    <div class="mt-4">
                        <h1 class="my-label">Known Languages</h1>
                        @foreach($user->languages_known as $language)
                            <h1 class="languages">{{$language}}</h1>
                        @endforeach
                    </div>

                    <div class="mt-4 flex justify-between">
                        <a href="{{ route('account.deleteprofilepic') }}" class="bt ">Remove Profile Pic</a>
                        <a href="{{ route('account.edit') }}" class="bt ">Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

