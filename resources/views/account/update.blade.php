<style>
    .card {
        width: 600px;
        margin: 10px auto;
        box-shadow: 1px 1px 5px grey;
        padding: 1.5rem;
        border-radius: 0.5rem;
    }
    .profilePic {
        height: 250px;
        margin: auto;
        border-radius: 10px;
    }
</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-right text-xl text-gray-800 leading-tight">
            <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
                {{ __('Add Post') }}
            </x-nav-link>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div  class="bg-white pb-3 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-gray-400 border-b border-gray-200">
                    <h1>Welcome {{ $user->name }} !!</h1>
                </div>
                <h1 style="font-size: 20px;" class=" py-5 text-center uppercase">My Account</h1>
                <div class="card bg-gray-200">

                    <form method="POST" action="{{ route('account.update') }}" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf
                    <div class="mt-4">
                        <img class="profilePic" src="/storage/images/profiles/{{$user->profilePic}}" class="profilePic" />
                    </div>
                    <!-- Name -->
                        <div>
                            <x-label for="name" :value="__('Name')" />

                            <x-input id="name"
                                     class="block mt-1 w-full @error('name') is-invalid @enderror"
                                     type="text" name="name" :value="$user->name" required autofocus />
                            @error('name')
                            <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <!-- Email Address -->
                        <div class="mt-4">
                            <x-label for="email" :value="__('Email')" />

                            <x-input id="email"
                                     class="block mt-1 w-full @error('email') is-invalid @enderror"
                                     type="email" name="email" :value="$user->email" required />
                            @error('email')
                            <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <!-- Username -->
                        <div>
                            <x-label for="username" :value="__('Username')" />

                            <x-input id="username"
                                     class="block mt-1 w-full @error('username') is-invalid @enderror"
                                     type="text" name="username" :value="$user->username" required autofocus />
                            @error('username')
                            <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <!-- Phone No -->
                        <div class="mt-4">
                            <x-label for="phone" :value="__('Phone')" />

                            <x-input id="phone"
                                     class="block mt-1 w-full @error('phone') is-invalid @enderror"
                                     type="text" :value="$user->phone"
                                     name="phone" required />
                            @error('phone')
                            <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <!-- Date of Birth -->
                        <div class="mt-4">
                            <x-label for="date_of_birth" :value="__('Date of Birth')" />

                            <x-input id="date_of_birth"
                                     class="block mt-1 w-full @error('date_of_birth') is-invalid @enderror"
                                     type="date" :value="$user->date_of_birth"
                                     name="date_of_birth" required></x-input>
                            @error('date_of_birth')
                            <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mt-4">
                            <x-label for="languages_known" :value="__('Known Languages')" />
                            <select id="languages_known" name="languages_known[]"
                                    class="block mt-1 w-full @error('languages_known') is-invalid @enderror" data-live-search="true" multiple>
                                <option value="hindi">Hindi</option>
                                <option value="english">English</option>
                                <option value="gujarati">Gujarati</option>
                            </select>
                            @error('languages_known')
                            <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <!-- Profile pic -->
                        <div class="mt-4">
                            <x-label for="profilePic" :value="__('Profile Pic')" />
                            <input type="file" name="profilePic" id="profilePic" class="block mt-1 w-full" />

                            @error('profilePic')
                            <span class="mt-3 list-disc list-inside text-sm text-red-600">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-4">
                                {{ __('Update') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

