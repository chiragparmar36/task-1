<style>
    .card {
        margin: 10px;
        box-shadow: 1px 1px 5px grey;
        padding: 1.5rem;
        border-radius: 0.5rem;
    }
    h1.title {
        font-size: 30px !important;
        margin-bottom: 5px;
    }

    h1.comment-title {
        font-size: 22px;
        margin-bottom: 10px;
    }
    h1.comment-details {
        font-size: 14px;
        margin-bottom: 10px;

    }
    h1.tags {
        margin-top: 10px;
    }
    h1.tags span {
        background: rgba(45, 55, 72, 0.57);
        padding: 5px 10px;
        border-radius: 5px;
        color: white;
    }
    .comments {
        border: 1px solid lightgrey;
        border-radius: 5px;
    }
    input.form-control {
        border-radius: 5px;
        border: 1px solid lightgrey;
        background: transparent;
    }
    .btn {
        display: block;
        color: white;
        padding: 10px 16px;
        border-radius: 5px;
        width: 100%;
        box-sizing: border-box;
        margin-left: 5px;
        cursor: pointer;
    }

    .btn.btn-reply {
        background: rgba(39, 78, 219, 0.87);
    }

    .btn.btn-edit {
        background: rgba(32, 83, 33, 0.87);
        height: 44px;
        padding: 0;
    }

    .btn.btn-delete {
        background: rgba(160, 13, 13, 0.87);
    }
    .user{{$user->id}} {
        background: #e5e7eb;
    }

    .error-message {
        color: red;
    }

    .editForm {
        display: none;
    }

    .profilePic {
        width: 100%;
        margin: 15px 10px;
        border-radius: 5px;
        box-shadow: 1px 5px 5px grey;
    }

    .btn.link {
        width: auto ;
    }

</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <x-slot name="header">
        <h2 class="font-semibold text-right text-xl text-gray-800 leading-tight">
            <x-nav-link :href="route('posts.create')" :active="request()->routeIs('dashboard')">
                {{ __('Add Post') }}
            </x-nav-link>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div  class="bg-white pb-3 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-gray-400 border-b border-gray-200">
                    <h1>Welcome {{ $user->name }} !!</h1>
                </div>
                <div class="p-3">
                    <div class="card bg-gray-200">
                        <h1 class="title uppercase">{{ $post->title }}</h1>
                        <h1>{{ $post->description }} </h1>
                        <h1 class="mt-5">Author: {{ $post->user->name }}</h1>
                        <h1>Posted at: {{ $post->user->created_at }}</h1>
                        <div class="flex justify-between">
                            <h1 class="tags">Tags:
                                @foreach( $post->tags as $tag)
                                    <span>{{$tag->name}}</span>
                                @endforeach
                            </h1>

                            @can('delete', $post)
                                <div class="flex justify-end">
                                    <form action="{{route('posts.delete', ['post' => $post->id])}}"  method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-delete w-full" type="submit">Delete Post</button>
                                    </form>
                                </div>
                            @elsecan('follow', $post)
                                <a class="btn-delete link btn" href={{route('unfollow', ['user_id' => $post->user_id])}} >Unfollow</a>
                            @else
                                <a class="btn-reply link btn" href={{route('follow', ['user_id' => $post->user_id])}} >Follow</a>
                            @endcan

                        </div>

                        @if(!empty($post->images))
                            <div class="flex">
                                @foreach( $post->images as $image)
                                    <img src="/storage/images/posts/{{$image}}" class="profilePic" />
                                @endforeach
                            </div>
                        @endif

                        <div class="comments mt-5 p-5 bg-white">
                            <form method="post" action="{{ route('comment.add') }}">
                                @csrf
                                <div class="form-group flex">
                                    <input type="text" name="comment_body" class="form-control w-full" placeholder="Add Comment...."/>
                                    <input type="hidden" name="post_id" value="{{ $post->id }}" />
                                    <input type="submit" class="btn link btn-reply w-40" value="Comment" />
                                </div>
                            </form>
                            <hr class="my-5" />
                            <h4 class="text-center uppercase">All Comments</h4>
                            @include('partials._comment_replies', ['comments' => $post->comments, 'post_id' => $post->id])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

