<style>
    .card {
        margin: 10px;
        box-shadow: 1px 1px 5px grey;
        padding: 1.5rem;
        border-radius: 0.5rem;
        position: relative;
    }
    h1.title {
        font-size: 25px;
        margin-bottom: 10px;
    }
    h1.tags {
        margin-top: 10px;
    }
    h1.tags span {
        background: rgba(45, 55, 72, 0.57);
        padding: 5px 10px;
        border-radius: 5px;
        color: white;
    }
    .{{$user->id}} {
        background: black;
    }
    .age_restricted {
        height: 30px;
        border-radius: 100%;
        position: absolute;
        top: 10px;
        right: 10px;
    }
</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-right text-xl text-gray-800 leading-tight">
            <x-nav-link :href="route('posts.create')" :active="request()->routeIs('dashboard')">
                {{ __('Add Post') }}
            </x-nav-link>
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
            <div  class="bg-white pb-3 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-gray-400 border-b border-gray-200">
                    <h1>Welcome {{ $user->name }} !!</h1>
                </div>
                <h1 style="font-size: 20px;" class=" py-5 text-center uppercase">All Posts</h1>
                <form action="{{ route('posts.index') }}" method="GET">
                    <div class="search flex justify-center px-8">
                        <input id="search" value="{{$search}}"
                                class="mr-2 w-full rounded" type="text" name="search"/>
                        <x-button type="submit">
                            {{ __('Search') }}
                        </x-button>
                    </div>
                </form>
                <div class="p-5">
                    @if($posts->isNotEmpty())
                        @foreach ($posts as $post)
                            <div class="card bg-gray-100 my-5">
                                @if($post->age_restricted)
                                <img src="/storage/images/18-plus-logo.jpeg" class="age_restricted" />
                                @endif
                                <a href={{route('posts.view', ['post' => $post->id])}}><h1 class="title uppercase">{{ $post->title }}</h1></a>
                                @if($post->user_id == $user->id)
                                    <h1>Author: (Your Post)</h1>
                                @else
                                    <h1>Author: {{$post->user->name}}</h1>
                                @endif
                                <h1 class="tags">
                                    @foreach( $post->tags as $tag)
                                        <span>{{$tag->name}}</span>
                                    @endforeach
                                </h1>
                                @if($errors->any() && ($errors->first() == $post->id))
                                    <h4 class="mt-3 text-red-500">This post is Age Restricted</h4>
                                @endif
                            </div>
                        @endforeach
                        <span>{{$posts->appends(request()->query())->links()}}</span>
                    @else
                        <div>
                            <h2>No posts found</h2>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>

