

<style>
    .desc {
        height: 200px;
        border: 1px solid lightgrey;
        border-radius: 5px;
    }
</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-right text-xl text-gray-800 leading-tight">
            <x-nav-link :href="route('posts.create')" :active="request()->routeIs('posts.create')">
                {{ __('Add Post') }}
            </x-nav-link>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-4xl mx-auto sm:px-6 lg:px-8">
            <div  class="bg-white pb-3 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-gray-400 border-b border-gray-200">
                    <h1 class="uppercase text-center text-2xl">Add Post</h1>
                </div>
                <div class="p-10">
                    <form method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
                    @csrf

                    <!-- Title -->
                        <div>
                            <x-label for="title" :value="__('Title')" />

                            <x-input id="title"
                                     class="block mt-1 w-full @error('title') is-invalid @enderror"
                                     type="text" name="title" :value="old('title')"  autofocus />
                            @error('title')
                            <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <!-- Description -->
                        <div class="mt-4">
                            <x-label for="description" :value="__('Description')" />

                            <textarea id="description" class="block mt-1 w-full desc @error('description') is-invalid @enderror"
                                      name="description" :value="old('description')"  ></textarea>
                            @error('description')
                            <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="mt-4">
                            <x-label for="tag" :value="__('Add Tags to post')" />
                            <x-input id="tag"
                                     class="block mt-1 w-full @error('tag') is-invalid @enderror"
                                     type="text" name="tag" :value="old('tag')" autofocus />
                            <span>Tags are separated by comma ','</span>
                        </div>

{{--                        @if($age_restricted)--}}
                            <div class="mt-4 flex">
                                <input type="checkbox" class="switch-input mr-3" value="1" id="age_restricted" name="age_restricted" />
                                <x-label for="age_restricted" :value="__('This Post is Age Restricted ?')" />
                            </div>
{{--                        @endif--}}

                        <div class="mt-4">
                            <x-label for="images" :value="__('Select Images')" />
                            <input type="file" name="images[]" id="images" class="block mt-1 w-full" multiple />

                            @error('images')
                            <span class="mt-3 list-disc list-inside text-sm text-red-600">{{ $message }}</span>
                            @enderror
                            @error('images.*')
                            <span class="mt-3 list-disc list-inside text-sm text-red-600">{{ $message }}</span>
                            @enderror
                        </div>

                        <x-button class="my-4">
                            {{ __('Add Post') }}
                        </x-button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


