@props(['value'])

<checkbox>
    {{ $value ?? $slot }}
</checkbox>
