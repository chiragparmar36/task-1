<style>
    .btn {
        width: 80px;
    }
</style>
@foreach($comments as $comment)
    <div class="display-comment pt-5 pl-10">
        <div class="user{{$comment->user_id}} p-4 border border-gray-300 rounded-2xl">
            <h1 class="comment-title">{{ $comment->body }}</h1>
            <h1 class="comment-details">
                By
                <span class="text-blue-900 font-bold uppercase">{{ $comment->user->username }}</span>
                <span class="text-green-900"> at {{$comment->created_at}}</span>
            </h1>
            <div class="flex">
                @if ($user->id != $comment->user_id)
                    <form class="replyForm" method="post" id="replyForm{{ $comment->id }}" action="{{ route('reply.add') }}">
                        @csrf
                        <div class="form-group flex">
                            <input type="text" name="comment_body" class="form-control"
                                   placeholder="Reply to {{$comment->user->username}}" required/>
                            <input type="hidden" name="post_id" value="{{ $post_id }}" />
                            <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
                            <input type="submit" class="btn btn-reply" value="Reply" />
                        </div>
                    </form>
                @elseif($comment->replies->count()==0)
                    <form method="post" class="editForm" id="editForm{{ $comment->id }}" action="{{ route('reply.edit', ['id'=> $comment->id])}}">
                        @csrf
                        @method('put')
                        <div class="form-group flex">
                            <input type="text" id="" name="comment_body" required value="{{$comment->body}}"
                                   class="form-control placeholder="Reply to {{$comment->user->username}}" />
                            <input type="submit" class="btn btn-reply" value="Update" />
                        </div>
                    </form>
                    <div class="flex">
                        <input type="button" id="btnedit{{$comment->id}}" onclick="editForm({{$comment->id}})" value="Edit" class="btn btn-edit"/>
                        <form id="deleteForm" action={{ route('reply.delete', ['id' => $comment->id]) }}  method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-delete" type="submit">Delete</button>
                        </form>
                    </div>
                @endif
            </div>
        </div>
        @include('partials._comment_replies', ['comments' => $comment->replies])
    </div>
@endforeach
<script>
    function editForm(id){
        if(document.getElementById('btnedit' + id).value == 'Edit') {
            document.getElementById('btnedit' + id).value = 'Back';
            document.getElementById('editForm' + id).style.display = 'block';
        }
        else {
            document.getElementById('editForm' + id).style.display = 'none';
            document.getElementById('btnedit' + id).value = 'Edit';
        }
    }

</script>
