<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\NotificationsController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Models\User;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    $users = User::withTrashed()->get();

    $currentUser = Auth::user();
//    return $currentUser;
    $notificationCount = Auth::user()->unreadNotifications()->count();
    return view('dashboard', [
        'currentUser' => $currentUser,
        'users' => $users,
        'notificationCount' => $notificationCount
    ]);
})->middleware(['auth'])->name('dashboard');


Route::group([ 'prefix' => '/posts', 'middleware' => 'auth' ], function() {
    Route::get('/', [PostController::class, 'index'])->name('posts.index');
    Route::get('/create', [PostController::class, 'create'])->name('posts.create');
    Route::post('/', [PostController::class, 'store'])->name('posts.store');

    Route::group(['prefix' => '/{post}'], function() {
        Route::get('/', [PostController::class, 'view'])->name('posts.view')->middleware('age');
        Route::delete('/', [PostController::class, 'delete'])->name('posts.delete');
    });

    Route::post('/comment/store', [CommentController::class,'store'])->name('comment.add');

    Route::group(['prefix' => 'reply'], function() {
        Route::post('/', [CommentController::class,'replyStore'])->name('reply.add');
        Route::put('/{id}', [CommentController::class,'replyEdit'])->name('reply.edit');
        Route::delete('/{id}', [CommentController::class,'replyDelete'])->name('reply.delete');
    });
});


Route::group([ 'prefix' => '/account', 'middleware' => 'auth' ], function() {
    Route::get('/', [AccountController::class, 'index'])->name('account.index');
    Route::get('/edit', [AccountController::class, 'edit'])->name('account.edit');
    Route::put('/', [AccountController::class, 'update'])->name('account.update');
    Route::get('/changepassword', [AccountController::class, 'changePassword'])->name('account.changepassword');
    Route::post('/changepassword', [AccountController::class, 'storeChangePassword'])->name('account.storechangepassword');
    Route::get('/deleteprofilepic', [AccountController::class, 'deleteProfilePic'])->name('account.deleteprofilepic');
});

Route::group(['prefix' => '/notifications', 'middleware' => 'auth' ], function() {
    Route::get('/', [NotificationsController::class, 'index'])->name('notifications');
    Route::get('/markasreadall', [NotificationsController::class, 'markAsReadAll'])->name('notifications.markasreadall');
    Route::get('/delete', [NotificationsController::class, 'delete'])->name('notifications.delete');
    Route::get('/markasread/{id}', [NotificationsController::class, 'markAsReadOne'])->name('notifications.markasread');
});

Route::get('/follow/{user_id}', [FollowController::class, 'follow'])->name('follow');
Route::get('/unfollow/{user_id}', [FollowController::class, 'unfollow'])->name('unfollow');

require __DIR__.'/auth.php';

