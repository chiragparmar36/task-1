<?php

use App\Http\Controllers\API\CommentController;
use App\Http\Controllers\API\PostController;
use App\Http\Controllers\API\AccountController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class, 'login']);
Route::post('register', [UserController::class, 'register']);
Route::post('refresh', [UserController::class, 'getRefreshToken']);

Route::group(['middleware' => 'auth:api' ], function() {

    Route::group([ 'prefix' => '/account' ], function() {
        Route::get('/', [AccountController::class, 'index'])->name('account.index');
    });

    Route::group([ 'prefix' => '/posts'], function() {

        Route::get('/', [PostController::class, 'index'])->name('posts.index');
        Route::post('/', [PostController::class, 'store'])->name('posts.store');

        Route::group(['prefix' => '/{post}'], function () {
            Route::get('/', [PostController::class, 'view'])->name('posts.view')->middleware('age');
            Route::delete('/', [PostController::class, 'delete'])->name('posts.delete');
        });

        Route::post('/comment/store', [CommentController::class,'store'])->name('comment.add');

        Route::group(['prefix' => 'reply'], function() {
            Route::post('/', [CommentController::class,'replyStore'])->name('reply.add');
            Route::put('/{id}', [CommentController::class,'replyEdit'])->name('reply.edit');
            Route::delete('/{id}', [CommentController::class,'replyDelete'])->name('reply.delete');
        });
    });

    Route::get('users', [UserController::class, 'users']);
    Route::post('logout', [UserController::class, 'logout']);
});
Route::get('/try', [UserController::class, 'try']);
