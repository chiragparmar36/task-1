<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
namespace App\Http\Controllers;
use App\User;
class UserController extends Controller
{
    public function destroy($id): \Illuminate\Http\RedirectResponse
    {
        $user = User::where('id', $id)->firstorfail()->delete();
        echo ("User Record deleted successfully.");
        return redirect('/');
    }
}
