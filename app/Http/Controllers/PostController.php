<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use App\Notifications\PostCreateNotification;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Collection;
use function Sodium\add;

class PostController extends Controller
{
    public function index(Request $request) {
        $user = Auth::user();
        $search = $request->query('search', '');
        $search = strtolower($search);
        $query = Post::query();
        if ($search) {
            $query->where('title', 'LIKE', "%{$search}%")
                ->orwhereHas('tags', function ($q) use ($request) {
                    $q->where('tags.name',strtolower($request->search) );
                });
        }
        $posts = $query->latest()->with('tags')->paginate(5);
        return view('post.posts', [
            'user' => $user,
            'posts'=> $posts,
            'search' => $search
        ]);
    }

    public function create() {
        $age_restricted = Auth::user()->age_restricted;
        return view('post.addPost', ['age_restricted' => $age_restricted]);
    }

    public function store(Request $request) {
        $request->validate([
            'title' => 'required',
            'images' => 'max:2',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,webp,svg|max:2048',
        ]);

        $post = new Post();
        $post->title = $request->title;
        $post->description = $request->description;
        $request->age_restricted ? $post->age_restricted = 1 : $post->age_restricted = 0;
        $images = [];
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {
                $filenameWithExt = $image->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $image->getClientOriginalExtension();
                $fileNameToStore = $filename.'_'. time().'.'.$extension;
                $path = $image->storeAs('public/images/posts', $fileNameToStore);
                $images[] = $fileNameToStore;
            }
        }
        $post->images = $images;
        $user = Auth::user();
        $user->posts()->save($post);
        $tags = explode(",", $request->tag);
        foreach ($tags as $tag) {
            $tag = trim($tag);
            $tag = strtolower($tag);
            $tagObj = Tag::firstOrCreate([
                'name' => $tag
            ]);
            $tagObj->posts()->attach($post->id);
        }

//        $user = Auth::user();
//        if($post->age_restricted) {
//            $age = Carbon::now()->addYears(-18);
//            $users = User::where(
//                'date_of_birth', '<', $age
//            )->get();
//        }
//        $tags = collect($tags);
//        $arr = [];
//        $users = User::whereJsonLength('interest', '>', 0)->get();
//        foreach ($users as $u) {
//            $interest = collect($u->interest);
//            $count = count($interest->intersect($tags));
//            if($count>0) {
//                $arr[] = $u->id;
//            }
//        }
//        $interest_users = User::whereIn('id', $arr)->get();
//
//        $users = collect($user->followers);
//        $diff = $users->diff(User::whereNotIn('id', $arr)->get());
//
//        $interest_users = $interest_users->except(Auth::id());
//        $users = $interest_users->merge($diff);
//
//        Notification::send($users,
//            new PostCreateNotification(
//                $user->username,
//                $post,
//            )
//        );
        return redirect()->route('posts.index');
    }

    public function view($post) {
        $user = Auth::user();
        return view('post.post', [
            'user' =>$user,
            'post'=> $post,
        ]);
    }

    public function delete($post) {
        $this->authorize('delete', $post);
        $deleted = Post::where('id', $post->id)->delete();
        if($deleted){
            return redirect()->route('posts.index');
        }
    }
}

