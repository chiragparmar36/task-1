<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Tag;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tags = Tag::get();
        return view('auth.register', ['tags' => $tags]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'unique:users', 'regex:/^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/'],
            'password' => ['required', 'confirmed', Rules\Password::min(8)],
            'phone' => ['required', 'string', 'regex:/^[0-9]{10}$/'],
            'date_of_birth' => ['required', 'date'],
            'languages_known' => ['required'],
            'interest' => ['required'],
            'profilePic' => 'image|mimes:jpeg,png,jpg,gif,webp,svg|max:2048',
        ]);

        if ($request->hasFile('profilePic')) {
            $filenameWithExt = $request->file('profilePic')->getClientOriginalName ();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('profilePic')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'. time().'.'.$extension;
            $path = $request->file('profilePic')->storeAs('public/images/profiles', $fileNameToStore);
        }
        else {
            $fileNameToStore = '';
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'date_of_birth' => $request->date_of_birth,
            'languages_known' => $request->languages_known,
            'interest' => $request->interest,
            'profilePic' => $fileNameToStore,
            'last_login' => date('Y-m-d H:i:s'),
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
