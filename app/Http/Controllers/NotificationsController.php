<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function index() {
        $user = Auth::user();
        $unreadNotifications = $user->unreadNotifications;
        $readNotifications = $user->readNotifications;
        $notificationCount = $user->unreadNotifications()->count();
//        dd($readNotifications, $unreadNotifications);
        return view(
            'notifications', [
                'notificationCount' => $notificationCount,
                'username' => $user->username,
                'unreadNotifications' => $unreadNotifications,
                'readNotifications' => $readNotifications
            ]
        );
    }
    public function markAsReadAll() {
        $user = Auth::user()->unreadNotifications->markAsRead();
        return redirect()->route('notifications');
    }

    public function delete() {
        $user = Auth::user()->readNotifications()->delete();
        return redirect()->route('notifications');
    }
    public function markAsReadOne($id) {
        $user = Auth::user()->unreadNotifications->find($id);
        $user->markAsRead();
        return redirect()->route('notifications');
    }
}
