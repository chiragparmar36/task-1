<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FollowController extends Controller
{
    public function follow($user_id) {
         $user = User::find($user_id);
         $user->followers()->attach(Auth::id());
         return back();
    }

    public function unfollow($user_id) {
        $user = User::find($user_id);
        $user->followers()->detach(Auth::id());
        return back();
    }
}
