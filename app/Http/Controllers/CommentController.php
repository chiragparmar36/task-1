<?php

namespace App\Http\Controllers;

use App\Jobs\SendComment;
use App\Jobs\SendReply;
use App\Mail\CommentCreate;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Notifications\CommentCreateNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Comment;
        $user = Auth::user();
        $comment->body = $request->comment_body;
        $comment->user()->associate($user);
        $post = Post::find($request->post_id);
        $post->comments()->save($comment);

        $postOwnerEmail = $post->user->email;

        if ($postOwnerEmail !== $user->email) {
            $username = $user->username;
            $users = User::find($post->user_id);
            SendComment::dispatch($username, $post);
        }

        return back();
    }

    public function replyStore(Request $request)
    {
        $request->validate([
            'comment_body' => ['required'],
        ]);
        $reply = new Comment();
        $user = Auth::user();
        $reply->body = $request->comment_body;
        $reply->source = $request->comment_id;
        $reply->user()->associate($user);
        $post = Post::find($request->post_id);
        $post->comments()->save($reply);

        $postOwnerEmail = $post->user->email;
        $username = $user->username;

        if ($postOwnerEmail !== $user->email) {
            SendComment::dispatch($username, $post);
        }

        $commentOwner = $reply->find($reply->source);
        SendReply::dispatch($username, $commentOwner);
        return back();
    }

    public function replyEdit(Request $request, $id)
    {
        $request->validate([
            'comment_body' => ['required'],
        ]);
        $comment = Comment::find($id);
        $comment->body = $request->comment_body;
        if($comment->save()) {
            return back();
        }
    }

    public function replyDelete($id)
    {
        $deleted = Comment::where('id', $id)->delete();
        if($deleted){
            return back();
        }
    }
}
