<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Jobs\SendComment;
use App\Jobs\SendReply;
use App\Mail\CommentCreate;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Notifications\CommentCreateNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'body' => ['required'],
        ]);
        $comment = new Comment;
        $user = $request->user();
        $comment->body = $request->body;
        $comment->user()->associate($user);
        $post = Post::find($request->post_id);
        $post->comments()->save($comment);

        $postOwnerEmail = $post->user->email;

        if ($postOwnerEmail !== $user->email) {
            $username = $user->username;
            $users = User::find($post->user_id);
            SendComment::dispatch($username, $post);
        }

        return new CommentResource($comment);
    }

    public function replyStore(Request $request)
    {
        $request->validate([
            'body' => ['required'],
        ]);
        $reply = new Comment();
        $user = $request->user();
        $reply->body = $request->body;
        $reply->source = $request->comment_id;
        $reply->user()->associate($user);
        $post = Post::find($request->post_id);
        $post->comments()->save($reply);

        $postOwnerEmail = $post->user->email;
        $username = $user->username;

        if ($postOwnerEmail !== $user->email) {
            SendComment::dispatch($username, $post);
        }

        $commentOwner = $reply->find($reply->source);
        SendReply::dispatch($username, $commentOwner);
        return new CommentResource($reply);
    }

    public function replyEdit(Request $request, $id)
    {
        $request->validate([
            'body' => ['required'],
        ]);
        $comment = Comment::find($id);
        $comment->body = $request->body;
        if($comment->save()) {
            return new CommentResource($comment);
        }
    }

    public function replyDelete($id)
    {
        $deleted = Comment::where('id', $id)->delete();
        if($deleted){
            return back();
        }
    }
}
