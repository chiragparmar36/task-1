<?php

namespace App\Http\Controllers\API;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Auth;
use Laravel\Passport\TokenRepository;
use Illuminate\Validation\Rules;

class UserController extends Controller
{
    public $successStatus = 200;
    private $client;

    public function __construct()
    {
        $this->client = Client::find(2);
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username' => ['required', 'unique:users', 'regex:/^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/'],
            'password' => ['required', Rules\Password::min(8)],
            'phone' => ['required', 'string', 'regex:/^[0-9]{10}$/'],
            'date_of_birth' => ['required', 'date'],
            'languages_known' => ['required'],
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'date_of_birth' => $request->date_of_birth,
            'languages_known' => $request->languages_known,
            'interest' => $request->interest,
            'last_login' => date('Y-m-d H:i:s'),
        ]);
        return $this->getTokenAndRefreshToken($request);
    }

    public function login(Request $request) {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            return $this->getTokenAndRefreshToken($request);
        }
        else {
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    private function getTokenAndRefreshToken(Request $request) {
//        $params = [
//            'grant_type' => 'password',
//            'client_id' => $this->client->id,
//            'client_secret' => $this->client->secret,
//            'username' => $request->email,
//            'password' => $request->password,
//            'scope' => '*',
//        ];
//        $request->request->add($params);
//        $proxy = Request::create('oauth/token', 'POST');
//        $response =  \Route::dispatch($proxy);
//        $content = json_decode($response->getContent(), true);
//        return $content;

        try {
            $response = Http::withHeaders(['Accept' => 'application/json'])->asForm()->post('http://127.0.0.1:8001/oauth/token', [
                'grant_type' => 'password',
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'username' => $request->email,
                'password' => $request->password,
                'scope' => '*',
            ]);

            $result = json_decode((string) $response->getBody(), true);
            return response()->json($result, $this->successStatus);
        }
        catch (Exception $e) {
            return response()->json("unauthorized", 401);
        }
    }

    public function getRefreshToken(Request $request) {
        $this->validate($request, [
            'refresh_token' => 'required'
        ]);
//        $params = [
//            'grant_type' => 'refresh_token',
//            'client_id' => $this->client->id,
//            'client_secret' => $this->client->secret,
//            'username' => request('email'),
//            'password' => request('password'),
//        ];
//        $request->request->add($params);
//        $proxy = Request::create('oauth/token', 'POST');
//        try {
//            $response = \Route::dispatch($proxy);
//            return  $response;
//        }
//        catch (Exception $e) {
//            return response()->json("unauthorized", 401);
//        }
        try {
            $response = Http::withHeaders(['Accept' => 'application/json'])->asForm()->post('http://127.0.0.1:8001/oauth/token', [
                'grant_type' => 'refresh_token',
                'refresh_token' => $request->refresh_token,
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => '*',
            ]);

            $result = json_decode((string) $response->getBody(), true);
            return response()->json($result, $this->successStatus);
        }
        catch (Exception $e) {
            return response()->json("unauthorized", 401);
        }
    }

    public function logout(Request $request) {
        if($request->user()->token()->revoke()) {
            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        }
        return response()->json(['error'=>'Unauthorised'], 401);
    }

    public function users() {
        return new UserCollection(User::all());
    }

}
