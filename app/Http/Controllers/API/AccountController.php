<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules;

class AccountController extends Controller
{
    public function index(Request $request) {
        $user = $request->user();
        return new UserResource($user);
    }

    public function edit() {
        $user = Auth::user();
        if (empty($user->profilePic)) {
            $user->profilePic = 'avatar.png';
        }
        return view('account.update', ['user' => $user]);
    }

    public function update(Request $request) {
        $user = Auth::user();
        $request->validate([
            'name' => ['required', 'regex:/^([a-zA-Z]+\s)*[a-zA-Z]+$/'],
            'email' => ['required', 'string', 'email', 'max:255',
                Rule::unique('users')->ignore($user->id, 'id')],
            'username' => ['required', 'regex:/^[A-Za-z0-9]+(?:[_-][A-Za-z0-9]+)*$/',
                Rule::unique('users')->ignore($user->username, 'username')],
            'phone' => ['required', 'string', 'regex:/^[0-9]{10}$/',
                Rule::unique('users')->ignore($user->phone, 'phone')],
            'date_of_birth' => ['required', 'date'],
            'languages_known' => ['required'],
            'profilePic' => 'image|mimes:jpeg,png,jpg,gif,webp,svg|max:2048',
        ]);

        if ($request->hasFile('profilePic')) {
            $filenameWithExt = $request->file('profilePic')->getClientOriginalName ();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('profilePic')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'. time().'.'.$extension;
            $path = $request->file('profilePic')->storeAs('public/images/profiles/', $fileNameToStore);
            Storage::delete('/public/images/profiles/'.$user->profilePic);
        }
        else{
            $fileNameToStore = $user->profilePic;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->phone = $request->phone;
        $user->date_of_birth = $request->date_of_birth;
        $user->languages_known = $request->languages_known;
        $user->profilePic = $fileNameToStore;
        $user->save();
        return redirect()->route('account.index');
    }

    public function deleteProfilePic() {
        $user = Auth::user();
        Storage::delete('/public/images/profiles/'.$user->profilePic);
        $user->profilePic = '';
        $user->save();
        return back();
    }

    public function changePassword() {
        return view('account.changepassword');
    }

    public function storeChangePassword(Request $request) {

        $request->validate([
            'password' => ['required', 'confirmed', Rules\Password::min(8)]
        ]);

        $user = Auth::user();
        if(Hash::check($request->oldpassword, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->route('account.index');
        }
        else {
            return back();
        }
    }
}
