<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public $successStatus = 200;

    public function index() {
        $posts = Post::get();
        return new PostCollection($posts);
    }

    public function view(Post $post) {
        return new PostResource($post);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'images' => 'max:2',
            'images.*' => 'image|mimes:jpeg,png,jpg,gif,webp,svg|max:2048',
        ]);

        $post = new Post();
        $post->title = $request->title;
        $post->description = $request->description;
        $request->age_restricted ? $post->age_restricted = 1 : $post->age_restricted = 0;
        $images = [];
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {
                $filenameWithExt = $image->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $image->getClientOriginalExtension();
                $fileNameToStore = $filename . '_' . time() . '.' . $extension;
                $path = $image->storeAs('public/images/posts', $fileNameToStore);
                $images[] = $fileNameToStore;
            }
        }
        $post->images = $images;
//        $user = $request->user();
        $request->user()->posts()->save($post);
        $tags = explode(",", $request->tag);
        foreach ($tags as $tag) {
            $tag = trim($tag);
            $tag = strtolower($tag);
            $tagObj = Tag::firstOrCreate([
                'name' => $tag
            ]);
            $tagObj->posts()->attach($post->id);
        }
        return new PostResource($post);
    }

    public function delete($post) {
        $this->authorize('delete', $post);
        $deletedPost = $post;
        if($post->delete()) {
            return new PostResource($deletedPost);
        }
        return response()->json(['error'=>'Something went wrong']);
    }
}

