<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;

class ageVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $age = auth()->user()->date_of_birth;
        $age = Carbon::parse($age)->age;
        $age_restricted = $request->post->age_restricted;
        if($age < 18 && $age_restricted) {
            return Redirect::back()->withErrors($request->post->id);
        }
        return $next($request);
    }
}
