<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'username',
        'date_of_birth',
        'languages_known',
        'profilePic',
        'interest',
        'last_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'languages_known' => 'array',
        'interest' => 'array',
        'last_login' => 'datetime',
    ];

    protected $dates = ['deleted_at'];

    public function posts() {
        return $this->hasMany(Post::class);
    }

    public function followers() {
        return $this->belongsToMany(User::class, 'user_follower', 'user_id', 'follower_id');
    }

    public function follows() {
        return $this->belongsToMany(User::class, 'user_follower', 'follower_id', 'user_id');
    }

}
