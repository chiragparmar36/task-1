<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Boolean;

class Post extends Model
{
    use HasFactory;

    public function comments() {
        return $this->hasMany(Comment::class)->whereNull('source');
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function tags() {
        return $this->belongsToMany(Tag::class, 'post_tag');
    }

    protected $casts = [
        'age_restricted' => 'boolean',
        'images' => 'array'
    ];
}
