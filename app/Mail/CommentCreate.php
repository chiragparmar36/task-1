<?php

namespace App\Mail;

use App\Models\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CommentCreate extends Mailable
{
    use Queueable, SerializesModels;

    protected $username;
    protected $postId;
    protected $postTitle;

    public function __construct($username, Post $post)
    {
        $this->username = $username;
        $this->postId = $post->id;
        $this->postTitle = $post->title;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mailLayout.comment')->with([
            'username' => $this->username,
            'title' => $this->postTitle,
            'post' => $this->postId,
        ]);
    }
}
