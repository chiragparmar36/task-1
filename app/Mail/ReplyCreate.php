<?php

namespace App\Mail;

use App\Models\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReplyCreate extends Mailable
{
    use Queueable, SerializesModels;

    protected $comment;
    protected $postId;
    protected $username;

    public function __construct($username, Comment $commentOwner)
    {
        $this->username = $username;
        $this->comment = $commentOwner->body;
        $this->postId = $commentOwner->post_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mailLayout.reply')->with([
            'username' => $this->username,
            'comment' => $this->comment,
            'post' => $this->postId,
        ]);
    }
}
