<?php

namespace App\Observers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use App\Notifications\PostCreateNotification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class PostCreateObserver
{
    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function created(Post $post)
    {
        $user = Auth::user();
        $all = User::get();
        $users = $user->followers;
        $diff = $all->diff($users)->where('interest', '!==', '');

        $diff = User::whereNotIn('id', $user->followers->pluck('id')->toArray())->get();

        $tags = explode(",", $this->request->tag);
        $tags = collect($tags);
        $arr = [];
        foreach ($diff as $u) {
            $interest = collect($u->interest);
            if(count($interest->intersect($tags))>0) {
                $arr[] = $u->id;
            }
        }
        $interest_users = User::whereIn('id', $arr)->get();
        $users = $interest_users->merge($users)->except(Auth::id());

        if($post->age_restricted) {
            $age = Carbon::now()->addYears(-18);
            $users = $users->where(
                'date_of_birth', '<', $age
            );
        }

        Notification::send($users,
            new PostCreateNotification(
                $user->username,
                $post,
            )
        );
    }

    /**
     * Handle the Post "updated" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        //
    }

    /**
     * Handle the Post "deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        //
    }

    /**
     * Handle the Post "restored" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function restored(Post $post)
    {
        //
    }

    /**
     * Handle the Post "force deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        //
    }
}
