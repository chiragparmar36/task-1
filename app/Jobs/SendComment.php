<?php

namespace App\Jobs;

use App\Mail\CommentCreate;
use App\Models\Post;
use App\Notifications\CommentCreateNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendComment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $username;
    protected $postOwnerEmail;
    protected $post;

    public function __construct($username, Post $post)
    {
        $this->username = $username;
        $this->postOwnerEmail = $post->user->email;
        $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->post->user->notify(
            new CommentCreateNotification(
                $this->username, $this->post
            )
        );
//        Mail::to($this->postOwnerEmail)->send(new CommentCreate($this->username, $this->post));
    }
}
