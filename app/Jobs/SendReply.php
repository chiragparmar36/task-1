<?php

namespace App\Jobs;

use App\Mail\ReplyCreate;
use App\Models\Comment;
use App\Models\Post;
use App\Notifications\ReplyCreateNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendReply implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $username;
    protected $commentOwner;

    public function __construct($username, Comment $commentOwner)
    {
        $this->username = $username;
        $this->commentOwner = $commentOwner;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->commentOwner->user->notify(
            new ReplyCreateNotification (
                $this->username,
                $this->commentOwner
            )
        );
    }
}
