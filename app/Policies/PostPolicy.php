<?php

namespace App\Policies;

use App\Models\Post;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

//    public function before(User $user, $ability)
//    {
//         if ($user->is_admin) {
//             return true;
//         }
//         return false;
//    }

    public function delete(User $user, Post $post)
    {
        return $user->id === $post->user->id;
    }

    public function follow(User $user, Post $post) {
        if($user->follows->firstWhere('id', '=', $post->user_id)) {
            return true;
        }
        return false;
    }

}
